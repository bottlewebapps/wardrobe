#! /usr/bin/env python3

"""
Module to pull outfits
"""

import random
import json

import my_data
categories = my_data.categories


def find_matches(clothes: list, field: str, value: str) -> list:
    '''
    Find maches for items in a clothes list with a given field/value
    As some values may be CSV we split the stored values into a list
    '''
    matches = []
    for item in clothes:
        if field in item and item[field] and value in item[field].split(','):
            matches.append(item)
    return matches


def pick_one(clothes: list, category: str) -> dict:
    '''
    Pick one (random) item for a given category from a list of (valid) clothes
    '''
    matches = find_matches(clothes, 'category', category)
    if matches:
        item = random.choice(matches)
        return item


def pull_outfit(constraints: dict = {}) -> list:
    '''
    Fetch all the clothes items from the database and return a list of items
    that meet the requested constraints.
    constraints example: {'season': 'spring', 'color': 'red'}
    The season may be also a CSV string i.e. "spring,summer"
    '''
    data = my_data.dump()
    # If the clothes inventory is empty just exit
    if not data or 'clothes' not in data:
        print("NO DATA IN OUTFIT, EXITING")
        return

    outfit = {}
    inventory = data['clothes']
    matches = inventory.copy()

    # List of matches for given constraints
    for constraint, value in constraints.items():
        matches = find_matches(matches, constraint, value)

    # Pick one item for each category
    for category in categories:
        item = pick_one(matches, category)
        if not item:
            print('No item found for ' + category +
                  ' picking up something from the rest')
            item = pick_one(inventory, category)
        if not item:
            print('No item found at all for ' + category)
            item = {}
        outfit[category] = item

    return outfit


if __name__ == '__main__':
    constraints = {'season': 'autum'}
    o = pull_outfit(constraints)
    # print(o['shoes'])
    print(json.dumps(o, indent=4))
