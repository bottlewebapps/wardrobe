#! /usr/bin/env python3

'''
Upload the test images and their data into the database of the webapp
by using HTTP requests
'''

import os
import json
import requests
import threading
import time

base_url = 'http://localhost:8080/entry'
sample_data_dir = './test_images'
image_extensions = ('.jpg', '.jpeg', '.png')


def upload_data(data: dict, image_file: str):
    with open(image_file, 'rb') as fh:
        files = {'img_file': fh}
        r = requests.post(base_url, files=files, data=data)


def insert_all():
    '''
    Search all images in the sample_data_dir and if they have a json file,
    upload them
    '''
    for entry in os.listdir(sample_data_dir):
        if entry.endswith(image_extensions):
            image_path = sample_data_dir + os.sep + entry
            data_file = sample_data_dir + os.sep + entry + '.json'
            if os.path.isfile(data_file):
                with open(data_file) as fh:
                    data = json.load(fh)
                # print("UPLOADING IMAGE:", image_path)
                # print("UPLOADING DATA:", data)
                upload_data(data, image_path)


# Same as insert_all but it runs in the background to avoid conflicts with
# the webapp
def insert_background():
    downloader_thread = threading.Thread(target=insert_all, name="inserter")
    downloader_thread.start()


if __name__ == '__main__':
    insert_all()
