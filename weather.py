#! /usr/bin/env python3

'''
Module to retrieve the current weather data from https://open-meteo.com
I rewrote this one because the meta-weather server seems to be down.
'''

import os
import json
import datetime
import requests


tmp_filename = 'tmp.weather.json'
time_format = '%d-%b-%Y (%H:%M:%S.%f)'

def bad_cache(maxhours: int = 24) -> None:
    '''
    Cache is "bad" if the stored temporal json file is older than "maxhours"
    This avoids asking for all the information on every web request
    '''
    if not os.path.exists(tmp_filename):
        return True

    with open(tmp_filename) as fh:
        data = json.load(fh)

    now = datetime.datetime.now()
    if 'timestamp' in data:
        ts_str = data['timestamp']
        ts = datetime.datetime.strptime(ts_str, time_format)
        diff = now - ts
        diff_h = diff.total_seconds() // 3600
        if diff_h < maxhours:
            return False
    return True


def get_coords(location = 'Bremen') -> dict:
    '''
    Get the GPS coordinates of a given city or address from the geocode API.
    Return a dictionary with longitude and latitude.
    '''
    url = 'https://geocode.xyz/' + location + '?json=1'
    x = requests.get(url)
    if x.status_code == 200:
        coords = {}
        tmp = x.json()
        # Only show two decimals, otherwise the API does not like it
        coords['latitude'] = "{:.2f}".format(float(tmp['latt']))
        coords['longitude'] = "{:.2f}".format(float(tmp['longt']))
        return coords


def weather(location = 'Bremen') -> dict:
    '''
    Get the weather information from the API
    '''
    coord = get_coords(location)
    coord_str = 'latitude=' + coord['latitude'] + '&'
    coord_str += 'longitude=' + coord['longitude']
    url = 'https://api.open-meteo.com/v1/forecast?' + coord_str 
    url += '&daily=temperature_2m_max&current_weather=true&'
    url += 'timezone=Europe%2FBerlin'

    x = requests.get(url)
    if x.status_code == 200:
        tmp = x.json()
        if 'current_weather' in tmp:
            ts = datetime.datetime.strftime(datetime.datetime.now(), time_format)
            cw = tmp['current_weather']
            cw['timestamp'] = ts
            with open(tmp_filename, 'w') as fh:
                json.dump(cw, fh)
            return cw

    print('ERROR: unable to get the weather information at the URL:')
    print('"' + url + "'")
    print('Response:')
    print(x.text)



def html_widget(location, online = False) -> str:
    '''
    Build the HTML code for the widget
    '''
    print("BK", bad_cache())
    if online or bad_cache():
        print('Retriving the weather information')
        f = weather(location)
    else:
        print('Reading cached data')
        with open(tmp_filename) as fh:
            f = json.load(fh)

    html = []
    html.append('<div id="weather_container">')
    #html.append('  <div>')
    #html.append('    <img src="' + icon_loc(f['weather_state_abbr'], online) +
    #            '" alt="' + f['weather_state_name'] + '">')
    #html.append('  </div>')
    html.append('  <div>')
    html.append('      <div>')
    html.append('         <div><strong style="font-size:28px;">&#x1F321;' +
                '</strong></div>')
    html.append('         <div>')
    html.append('             <div>' + str(int(f['temperature'])) + ' °C</div>')
    html.append('         </div>')
    html.append('      </div>')
    html.append('      <div>')
    html.append('        <div>')
    html.append('          <div><strong style="font-size:15px;">&#127811;' +
                '</strong> ' + str(int(f['windspeed'])) + ' km/h </div>')
    html.append('        </div>')
    html.append('      </div>')
    html.append('  </div>')
    html.append('</div>')

    return '\n'.join(html)


if __name__ == '__main__':
    print(weather())
