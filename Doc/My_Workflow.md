# My Worlflow

Here I am going to describe how I wrote this app, from beginning to end, from a strategic point of view. Without too many technical details.

Usually a software development process is quite iterative, which involves:

1. Writing some little tool
2. Writing another tool, and finding out that I need an extra feature from the first one. So, go back and do it. Then complete this tool.
3. Write another one, and maybe do some adjustments in the previous ones.
4. Eventually finding out that I can do the whole thing in a way more simple and better way, so I go back and rewrite everything.
5. Once the whole thing starts taking shape, I find out that it doesn't look so good, so I go back and clean up unused parts, write better comments, ...
6. Once I think that everything is looking fine, I use the app a little bit, and find out that the there would be some extra nice features I would like to have. So, I go back and implement them.

I am going to describe what I did for this webapp:

## Figure out the idea

One of the most challenging parts is to think of something we somehow feel motivated about and figure out how an app would help us achieve it.

In this case, I thought that it could be useful to have **a system to pick up outfits**.

Then I thought about the features this system should have. For example...
- An inventory with a lot of clothes
- This inventory should have features for each item, to help us match outfits
- A simple database like SQLite would work for storing this data
- We could create an stand-alone tool to store, retrieve and delete the data from the database
- We need another tool to filter items from the inventory and pick an outfit
- We need a web interface to be able to visualize the results
- The web interface will be served by Python Bottle, which will turn the user interaction with the web into system actions, for example, pick an outfit
- The web interface should have 3 sections: one for visualizing the outfit, one for entering new items, and one for visualizing the inventory and deleting items
- It would be cool to have a way to check the current local weather forecast and make the choices according to that


## The database tool

Before even thinking about the web interface, I started writing the database to handle the data. I called it *my_data.py*.

Before coding anything, I thought about which features would I need to store.
- In SQL database, we need a unique index *id* to address each entry
- We need a reference to the image file (*image_file*), or path in the file-system
- Some classification for pants, shoes, ... I called it *category*
- Color... or course
- Some hint on the best time of the year to use it, for example the *season*
- Wether it is something for work, sport... *occasion*
- I thought that knowing is *waterproof* could come in handy at some point
- And *other* in case there is something I forgot about

The first feature should be to be able to ensure that the database file and table are in place, containing the fields listed above. If not create them.

In order to simplify the database operations, I create a function called *sql_run*, which runs commands on a given database (file).

The SQL operations are usually done by two (or more) functions, one for inserting a new entry and another one for updating, but in order to simplify I created a function called *insert* which does both.

A *dump* function is also always convenient, specially for such a small database, in order to take a quick overview of all the data stored, without having to use some external database visualization tool (like SQLiteStudio).

Finally, I made the module able to be executed on command line as well as imported as a module in other Python scripts. That is done with the last 2 lines: `if __name__ == '__main__': ...`, and a wrapper function to handle the command line arguments.


## Draft of the web GUI

To design the web GUI, I took a piece of pape (whiteboard is also good), draw a big square, and draw more or less the elements I would like to have, in the place and shape I wanted. That for each page in our webapp, in this case *main page*, *new entry* and *inventory*.

I stared by the inventory one, to be able to visualize the data store on the database.
Once it is done, write some HTML draft on a stand-alone HTML file (*.html*) and keep trying things and visualizing the result on a web browser.

Something like this:

```html
<!DOCTYPE html>
<!-- This is an HTML comment -->
<head>
    <title>Page title</title>
    <link rel="stylesheet" href="/static/style.css">
</head>

  <body>

    <h2>Inventory</h2>

    <table>
       <tr>
         <th>id</th>
         <th>Image file</th>
         <th>Category</th>
         <th>Color</th>
         <th>season</th>
       </tr>

       <tr>
         <td>1</td>
         <td>PATH/TO/FILE1.png</td>
         <td>shoes</td>
         <td>black</td>
         <td>winter</td>
       </tr>

       <!-- Repeat the last block a couple of times with different values -->

    </table>

  </body>

</html>

```

  So, in order to use that file, store it in a file called, for example `mytest.html` and double click on it or open it somehow with a web browser (i.e. Firefox).


## Draft of the back-end

In the previous step I wrote an static inventory page. As a first approach we can try to serve that same static page with Python and Bottle.

This could be simply done as follows:

- Write the Webapp main file, which we call `app.py` by convention, with the following content:

```python
from bottle import route, run, template

@route('/inventory')
def intentory():
    return template(inventory)

run(host='127.0.0.1', port=8080)
```

- Create a Bottle template by simply creating a folder called `views` and moving the `mytest.html` file from previous step into it, and renaming it as `inventory.tpl`.

- Test the app by calling it from a terminal as: `python app.py` and opening a browser at <http://localhost:8080/inventory>


## Start filling up the template with "real" data

I could build the inventory table directly on the Bottle template or by passing it as string. I chose the second option, and created the `html_functions.py` module with the function `html_table()`, which generates an HTML table from the data in the database and returns it as a string.

So, at this point, the `inventory.tpl` file can already be simplified, by substituting everything between `<table>` and `</table>` with `{{!table}}`, where `table` is the variable containing the string with the HTML table, and the *!* sign tells that the variable should be printed literally, otherwise the characters would get modified (escaped).

And of course... We have to pass the `table` variable to the template, so we rewrite `app.py` as:

```python
from bottle import route, run, template

@route('/inventory')
def intentory():
    data = my_data.dump()
    table = web_tools_html_table(data)
    return template(inventory, table=table)

run(host='127.0.0.1', port=8080)
```

If we test the webapp again, it should show the table with the data in the database.


## A page for adding new entries

In principle, this step could be done with a simple static form on the `/entry` route, but what about if we decide to add more fields to our database, like "brand" or "material"? ... Then we would have to rewrite the new entry form, and many other things.

For that reason, on the `my_data.py` file I defined a dictionary called `attr` (attributes) for each field. For each attribute there are multiple options, including:

- *choices*: in case the user can only chose between a finite amount of options. This is convenient to ensure that the syntax is coherent for all items, and avoid items with `season: spring` and another one with `season: Spring`, so if we search for `spring` we only get one, or have to do tricks for fix cases and so on. Another one is the *color*, where a color could have hundreds of variants, which would be tricky to compare, but well... A blue shirt combines good with some blues shoes, no matter if they are light blue, dark blue, mint, ... Or whatever. If somebody really needs more colors, then just add them (as valid HTML color names) into the `colors` tuple
- *multiple*: whether the user is allowed to chose more than one. I.e. A jacket can be good for autum and winter, so season is "multiple", but waterproof is either true or false
- *input_type*: the type of HTML input element used for this field
- *required*: to ensure that the user does not submit the *new entry* form without filling up this field

With this dictionary, we can generate the *new entry* form dynamically. In this case, I generated the form embedding Python code inside the Bottle template `entry.tpl`.

Nowadays file-upload forms usually support "Drag-and-Drop" function, so I built that in. Usually this is done with javascript, but I found a really cool way to do it with pure CSS. The CSS part is, as all other CSS definitions, stored in the `static/style.css` file. All the testing for this feature ware already done in the [Draft of the web GUI]{## Draft of the web GUI} section.

So, at this point, we basically just need to take the draft of the static HTML form and substitute with with loops on the `entry.tpl` file.


## Duplicated POST variables

When submitting the data of the *new entry* form, the `checkbox` input elements produce duplicate values for some variables. In order to be able to properly parse all the values for a given variable, I created the little `bottle_helper.py` module, with the function `merge_post()` function. This function takes all the key/value pairs received and merges them into a comma-separated-values (CSV) string.


## Too to pick an outfit

We already have the data in the database, but we need a tool to take that data and chose the actual outfit, with all the pieces (or as many as possible). That's what the `ouftit.py` module does.

The raw data from the database (obtained from `my_data.dump()`) will be a list of dictionaries, and we need to filter items according to our criteria. That is what the `find_matches()` function does.

However, the `find_matches()` function returns a list of valid items, and at the end, we just want one, so that is what the `pick_one()` function is for. It "picks" one item for a given category (shoes, socks, ...)

And the main function, which uses the previous one is called `pull_ouftit()`. It accepts a dictionary of constraints, to be used as filter, for example: `{'season': 'summer', 'color': 'blue'}`.


## The main page

The main page should show a column with top to bottom items. Starting from *hat*, *jacket*, ... and the *shoes* on the bottom.

That was easily implemented on the `mainpage.tpl` template.

The items on the outfit to be displayed where chosen with the `outfit` module and passed to the Bottle template within the *outfit* key of the *data* dictionary.


## The weather widget

I wanted to embed some real-time weather data on the website, so I created the `weather.py` module, which works stand-alone, from command line, and can be imported to generate the *html_widget* to embed directly on the Bottle template as string.

Initially I thought of parsing some weather forecast website, but it was too tricky. But then I found out a free weather API which works without registration, so I used the data (and weather icons) from there.


## Test data

The app makes no fun without having enough items to pick from, and in order to avoid having to enter each item on the *new entry* form by hand, I created the `upload_test_data.py` module, which again, works as a stand-alone tool from command line, or can be imported as a module in Python.

I found out that using the `insert_all()` function works find as a stand-alone tool, from command line, but it gets stuck when calling it from the app, so I created the `insert_background()` function, which does the same as `insert_all()` but sending the task to the background.

For this reason, after calling the `/load_test_data` route on the browser, it will wait a little bit until the task is finished before reloading the inventory.

## Import/Export

The module to upload the test data via HTTP requests works fine, but it would be also nice to have a way to do back-ups and restores. For that, I wrote the `data_transfer.py` module, which can export and import the data internally, by directly copying the image to the target directory and retrieving/inserting the data directly on the database via `my_data.py` module. This way, we avoid having to process each item through the web-app individually.


## Code quality

There are many ways to write Python code that simply work. 
However, professional code usually follows some style standards. We can check if our code is properly written by running a *pycodestyle* (previously *pep8*) test. The pycodestyle package can be installed by `pip install pycodestyle`.
