#! /usr/bin/env python3
'''
Helper module to handle HTML
'''

def html_table(data:list)->str:
    '''
    Take list of clothes (dictionaries) and turn them into an HTML table.
    '''
    #fields = list(my_data.layout.keys())
    # If the inventory is empty return 
    if not len(data) or 'clothes' not in data:
        return ''

    table_name = list(data.keys())[0]
    rows = data[table_name]
    fields = list(rows[0].keys())

    html = []
    html.append('<h2>' + table_name + '</h2>')
    html.append('<table>')

    # Header
    html.append('  <tr>')
    for field in fields:
        html.append('    <th>' + field.capitalize() + '</th>')
    html.append('    <th>' + ''+ '</th>')
    html.append('  </tr>')

    # Rows
    for row in rows:
        html.append('  <tr>')
        idx = row['id'] if 'id' in row else None
        for field in fields:
            value = row[field] if field in row else ''
            html.append('    <td>' + str(value) + '</td>')
        # Add a little thumbnail image of the item
        image_file = row['image_file'].lstrip('.')
        html.append('    <td><img src="' + image_file + '" alt="Thumbnail of item with Id ' + str(row['id']) + '" height="40"></td>')
        # delete function
        html.append('    <td><a href="/delete?idx=' + str(idx) + '">Delete</a></td>')
        html.append('  </tr>')

    html.append('</table>')
    html_str = '\n'.join(html)
    return html_str


if __name__ == '__main__':
    import my_data
    test_data = my_data.dump()
    if test_data and 'clothes' in test_data:
        test_data = test_data
    #print(test_data)

    d = html_table(test_data)
    print(d)
