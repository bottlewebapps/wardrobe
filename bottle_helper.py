#! /usr/bin/env python3
"""
Module with convenient functions to handle bottle tasks
"""


from bottle import request

from my_data import layout, attr


def merge_post(POST, CSV: bool = True) -> dict:
    """
    Flatten a POST dataset by turning duplicate values into lists,
    or CSV strings
    """
    out = {}
    for field in layout.keys():
        tmp = POST.getall(field)
        if tmp:
            if 'multiple' in attr[field] and attr[field]['multiple']:
                if CSV:
                    val = ','.join(tmp)
                else:
                    val = tmp
            elif len(tmp) == 1:
                val = tmp[0]
            out[field] = val

    return out
