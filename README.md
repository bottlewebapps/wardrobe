# Wardrobe

This is a Webapp for chosing outfits from a database.

The back-end is written in Python and Bottle and the front-end in HTML, CSS and some Javascript.


# How to use

If in Linux (Ubuntu) we need some dependencies to build Pillow:

    sudo apt-get install libjpeg-dev zlib1g-dev

Install the Python dependencies by running the following command on a terminal:

    python -m pip install -r requirements.txt -U

Run the app from a terminal by typing:

    python app.py

This will start the webapp daemon and keep showing new lines every time we visit the webapp.

Open a browser (i.e. Firefox) and visit the address <http://localhost:8080>. If everything worked out, you should see the webapp.

