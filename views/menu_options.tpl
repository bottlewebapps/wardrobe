
<div id="options_widget" class="dropdown">

  <button class="options_btn"> Search options</button>
  <div id="options" class="dropdown-content">
    <form method="POST">
  
    <p> Season:
    % for i, season in enumerate(['spring', 'summer', 'autum', 'winter']):
        <label for="season_{{i}}">{{season}}</label>
        <input type="checkbox" id="season_{{i}}" name="season" value="{{season}}">
    % end
    <br>
    
    <input type="submit" value="Submit">
    </form>
  </div>

</div>
