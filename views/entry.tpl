% include('header')
% include('navigation')

<form method="POST" enctype="multipart/form-data">

<div class="file-area">
    <label for="img_file_l">Cloth image file:</label>
    <input type="file" id="img_file_l" name="img_file" required>
    <div class="file-dummy">
        <span class="default">Click to select a file, or drag it here</span>
        <span class="success">Great, your file is selected</span>
    </div>
</div>

<br>
    % for at, props in attr.items():
        <div id="choices">
        % required = 'required' if 'required' in props else ''
        % if 'choices' in props:
            <p>{{at.capitalize()}}:</p>
            % for i, choice in enumerate(props['choices']):
                % label_extra = ''
                % if at == 'color':
                    % label_extra = ' <span style="background-color:' + choice + ';">_</span>'
                % end
            <label for="{{at}}_{{i}}">{{choice}}</label>{{!label_extra}}
            <input type="{{props['input_type']}}" id="{{at}}_{{i}}" name="{{at}}" value="{{choice}}" {{required}}>
            % end
        % else:
            <label for="{{at}}">{{at.capitalize()}}: </label>
            <input type="{{props['input_type']}}" id="{{at}}" name="{{at}}">
        % end
        </div>
    % end

<br>

    <input type="submit" value="Submit">
</form>

% include('footer')
