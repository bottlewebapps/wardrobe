% include('header')
% include('navigation')

% if table:
{{!table}}
% else:
  <h3 style="color:red;"> WARNING: The closet is empty.</h3>
  <a href="/load_test_data">Load some test data</a>
% end

% include('footer')
