<head>
  <meta http-equiv="refresh" content="5; URL=/inventory" />
</head>
<body>

<script>
var timeleft = 5;
var downloadTimer = setInterval(function(){
  if(timeleft <= 0){
    clearInterval(downloadTimer);
    document.getElementById("progressBar").value = 5 - timeleft;
    document.getElementById("countdown").innerHTML = "Finished";
  } else {
    document.getElementById("countdown").innerHTML = timeleft;
  }
  timeleft -= 1;
}, 1000);

</script>
  <p>Redirecting to inventory in <span id="countdown">5</span> seconds. Otherwise, <a href="/inventory">click here</a>.</p>
<progress value="0" max="5" id="progressBar"></progress>  

</body>
