#! /usr/bin/env python3

"""
Module with functions to work with images
"""
import sys
from PIL import Image

def get_size(image_file)->tuple:
    im = Image.open(image_file)
    return im.size


if __name__ == '__main__':
    s = get_size(sys.argv[1])
    print("Size:", s)
