#! /usr/bin/env python3
'''
## THIS MODULE IS NOW OBSOLETED
## THE API DOES NOT SEEM TO BE ONLINE ANYMORE!

Module to retrieve the current weather data from https://www.metaweather.com
In order to avoid making too many web requests to the API, and posibly getting
blocked, I cache the data and only refresh if implictly asked or if the data
is older than one day.
It also generates the HTML code (as string) to be embeded directly on the
website.
The `get_icons()` function downloads all the weather icons to be used on the
widget.
The default city is "Bremen", because I live here, but feel free to change it.
'''


import os
import sys
import json
import requests
import datetime

base_url = 'https://www.metaweather.com'
tmp_filename = 'tmp.weather.json'
time_format = '%d-%b-%Y (%H:%M:%S.%f)'


def woeid(city: str = 'bremen') -> str:
    url = base_url + '/api/location/search/?query=bremen'
    x = requests.get(url)

    out = x.json()
    return out[0]['woeid']


# Relevant properties
props = 'min_temp max_temp humidity wind_speed weather_state_name'
props += ' weather_state_abbr'
props = props.split()


def bad_cache(maxhours: int = 24) -> None:
    '''
    Cache is "bad" if the stored temporal json file is older than "maxhours"
    '''
    if not os.path.exists(tmp_filename):
        return True

    with open(tmp_filename) as fh:
        data = json.load(fh)

    now = datetime.datetime.now()
    if 'timestamp' in data:
        ts_str = data['timestamp']
        ts = datetime.datetime.strptime(ts_str, time_format)
        diff = now - ts
        diff_h = diff.total_seconds() // 3600
        if diff_h < maxhours:
            return False
    return True


def weather(woeid: int = woeid('bremen'), online: bool = False):
    '''
    Pull the weather information.
    If the online option is False or the cache data is bad, pull it from the
    internet.
    Otherwise just read the cache file (to optimize network usage)
    '''
    if online or bad_cache():
        print('Pulling weather data from: ' + base_url)
        url = base_url + '/api/location/' + str(woeid)
        x = requests.get(url)
        out = x.json()
        cw = out['consolidated_weather'][0]
        ts = datetime.datetime.strftime(datetime.datetime.now(), time_format)
        cw['timestamp'] = ts
        with open(tmp_filename, 'w') as fh:
            json.dump(cw, fh)
        return cw
    else:
        with open(tmp_filename) as fh:
            data = json.load(fh)
        return data


# w = woeid('bremen')
# f = weather(w)
# print(f)

# Weather abbreviations
w_abbr = "sn sl h t hr lr s hc lc c".split()
icon_dir = './static/icons'


def icon_loc(abbr: str, online: bool = True) -> None:
    """
    Location of the weather icon
    """
    if online:
        url = base_url + '/static/img/weather/' + abbr + '.svg'
        return url
    else:
        path = icon_dir + os.sep + abbr + '.svg'
        return path


def get_icons():
    """
    Download all Weather icons
    """
    if not os.path.isdir(icon_dir):
        os.makedirs(icon_dir)

    for abbr in w_abbr:
        url = icon_loc(abbr, True)
        filename = icon_loc(abbr, False)
        with open(filename, 'wb') as f:
            f.write(requests.get(url).content)


def html_widget(city: str, online: bool = False):
    '''
    Build the HTML code for the widget
    '''
    wid = woeid('bremen')
    f = weather(wid)
    html = []
    html.append('<div id="weather_container">')
    html.append('  <div>')
    html.append('    <img src="' + icon_loc(f['weather_state_abbr'], online) +
                '" alt="' + f['weather_state_name'] + '">')
    html.append('  </div>')
    html.append('  <div>')
    html.append('      <div>')
    html.append('         <div><strong style="font-size:28px;">&#x1F321;' +
                '</strong></div>')
    html.append('         <div>')
    html.append('             <div>' + str(int(f['min_temp'])) + ' °C</div>')
    html.append('             <div>' + str(int(f['max_temp'])) + ' °C</div>')
    html.append('         </div>')
    html.append('      </div>')
    html.append('      <div>')
    html.append('        <div>')
    html.append('          <div><strong style="font-size:15px;">&#128167;' +
                '</strong> ' + str(f['humidity']) + ' %</div>')
    html.append('          <div><strong style="font-size:15px;">&#127811;' +
                '</strong> ' + str(int(f['wind_speed'])) + ' km/h </div>')
    html.append('        </div>')
    html.append('      </div>')
    html.append('  </div>')
    html.append('</div>')

    return '\n'.join(html)


if __name__ == '__main__':
    print(weather(online=False))
    # print(html_widget('bremen'))
